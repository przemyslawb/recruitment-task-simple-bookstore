import {
    Home,
    ShippingCard,
    OrderSummary
} from '../views/pages/index'

const routes = [
    {
        path: '/',
        component: Home,
        exact: true
    },
    {
        path: '/shopping-cart',
        component: ShippingCard,
        exact: true
    },
    {
        path: '/order-summary',
        component: OrderSummary,
        exact: true
    }
];

export default routes;
