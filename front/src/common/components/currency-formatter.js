import React from 'react';

import Currency from 'react-currency-formatter';

const convertPolishCurrency = value => value / 100;

// Na potrzeby tej aplikacji zdecydowałem się na takie rozwiązanie, jestem świadom, że nie jest ono najbardziej optymalne.
const CurrencyFormatter = ({ quantity }) => {
    const currency = convertPolishCurrency( quantity );

    return (
        <Currency quantity={ currency }
                  currency='PLN'
                  locale='pl'
                  decimal=','
                  group=',' />
    )
};

export default CurrencyFormatter;
