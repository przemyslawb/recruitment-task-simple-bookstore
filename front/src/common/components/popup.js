import React from 'react';
import { func, bool } from 'prop-types';

import Snackbar from '@material-ui/core/Snackbar';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';

const anchorOrigin = {
    vertical: 'bottom',
    horizontal: 'left',
};

const Popup = ({ message, handleClose, isOpen }) => (
    <Snackbar anchorOrigin={ anchorOrigin }
        open={ isOpen }
        autoHideDuration={ 6000 }
        onClose={ handleClose }
        message={ message }
        action={[
            <Button key='close'
                        aria-label='Close'
                        color='inherit'
                        onClick={ handleClose } >
                <CloseIcon />
            </Button>,
        ]}
    />
);

Popup.propTypes = {
    isOpen: bool.isRequired,
    handleClose: func
};

export default Popup;
