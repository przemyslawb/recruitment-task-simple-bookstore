import React from 'react';

import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';

export const FormSelect = ({ input, label, meta: { touched, error }, children, ...custom }) => (
    // Ten element nie powinien zawierać stylowania inline'wego, ale niestety zadecydowała tutaj presja czasu.
    <FormControl style={{ width: '100%' }}>
        <InputLabel >{ label }</InputLabel>
        <Select { ...input }
                { ...custom } >
            { children }
        </Select>
    </FormControl>
);

export const FormField = ({ input, meta: { touched, error }, ...props } = {}) => (
    <div>
        <TextField { ...input }
                   {  ...props }
                   error={ touched && Boolean( error ) } />
        { touched && error && <FormHelperText>{ error }</FormHelperText>}
    </div>
);
