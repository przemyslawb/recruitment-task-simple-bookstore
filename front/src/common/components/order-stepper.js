import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core';

import Step from '@material-ui/core/Step/Step';
import StepLabel from '@material-ui/core/StepLabel/StepLabel';
import Stepper from '@material-ui/core/Stepper/Stepper';

const styles = theme => ({
    button: {
        marginRight: theme.spacing.unit,
    },
    instructions: {
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit,
    },
});

const renderStep = (label, index) => (
    <Step key={ index } >
        <StepLabel>{ label }</StepLabel>
    </Step>
);

const OrderStepper = ({ activeStep, steps, children }) => (
    <Fragment>
        <Stepper activeStep={ activeStep }>
            { steps.map( renderStep ) }
        </Stepper>
        { children }
    </Fragment>
);

export default withStyles(
    styles
)( OrderStepper );
