import { length, equals } from 'ramda';
import { ZIP_CODE_REGEXP, EMAIL_REGEXP } from './constants'

export const required = value => value ? undefined : 'Pole wymagane';

export const emailValidation = value => EMAIL_REGEXP.test( value ) ? undefined : 'Nieprawidłowy adres e-mail';

export const zipCodeValidation = value => ZIP_CODE_REGEXP.test( value ) ? undefined : 'Nieprawidłowy kod pocztowy';

export const lengthValidation = lengthValue => value => equals(length(value), lengthValue) ? undefined : 'Nieprawidłowa długość';
