import React, { Component as ReactComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import { pipe } from 'ramda';
import LinearProgress from '@material-ui/core/LinearProgress/LinearProgress';

function withSpinner({
    isLoadingAction,
    loadingComponent: Loading = () => <LinearProgress />,
    isEmptyAction,
    emptyComponent: Empty = () => <p>No results</p>,
    fetch
} = {}) {
    return WrappedComponent => class WithSpinner extends ReactComponent {
        componentDidMount() {
            const { props } = this;

            fetch( props )
        }

        render() {
            const { props } = this;

            return (
                <Fragment>
                    { isLoadingAction( props ) && <Loading /> }
                    { isEmptyAction( props ) ? <Empty /> : <WrappedComponent  { ...props } /> }
                </Fragment>
            )
        }
    }
}

export default ( withDataParams, ...connectParams ) => pipe(
    withSpinner( withDataParams ),
    connect( ...connectParams )
)
