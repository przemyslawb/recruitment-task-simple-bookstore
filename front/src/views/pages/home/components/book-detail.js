import React from 'react';
import { withStyles } from '@material-ui/core';
import { Link } from 'react-router-dom';

import Card from '@material-ui/core/Card/Card';
import CardMedia from '@material-ui/core/CardMedia/CardMedia';
import CardContent from '@material-ui/core/CardContent/CardContent';
import Typography from '@material-ui/core/Typography/Typography';
import Button from '@material-ui/core/Button/Button';
import Grid from '@material-ui/core/Grid/Grid';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import CardActionArea from '@material-ui/core/CardActionArea/CardActionArea';

import CurrecnyFormatter from '../../../../common/components/currency-formatter';

// Użyłem stylowania w tym miejscu, ponieważ stylowanie tego elementu nie dotyczy kontenera.
const styles = () => ({
    card: {
        width: '100%',
        height: '100%',
        display: 'flex'
    },

    media: {
        height: 'auto',
        width: 140,
        margin: 20
    },

    buttonLabel: {
        textAlign: 'center'
    },

    buttonsContainer: {
        padding: 10,
        height: '35%'
    }
});

const BookDetail = ({ classes, addToShoppingCart, book }) => (
    <Grid item
          xs={ 6 }>
        <Card className={ classes.card }>
            <CardActionArea>
                <CardContent>
                    <Typography component='p'>
                        { book.title }
                    </Typography>
                    <Typography variant='caption'>
                        { book.author }
                    </Typography>
                </CardContent>
                <CardMedia component='img'
                           className={ classes.media }
                           src={ book.cover_url }
                           title={ book.title } />
            </CardActionArea>
            <Grid container
                  direction='column'
                  justify='space-around'
                  alignItems='stretch'>
                <CardContent>
                    <Typography component='p'>
                        Liczba stron:
                    </Typography>
                    <Typography variant='caption'>
                        { book.pages }
                    </Typography>
                    <Typography/>
                    <Typography component='p'>
                        Cena:
                    </Typography>
                    <Typography variant='caption'>
                        <CurrecnyFormatter quantity={ book.price } />
                    </Typography>
                </CardContent>
                <Grid container
                      direction='column'
                      justify='space-between'
                      className={ classes.buttonsContainer }>
                    <Button size='small'
                            variant='outlined'
                            color='primary'
                            component={ Link }
                            to='/shopping-cart'>
                        <Grid item
                              xs={ 11 }
                              className={ classes.buttonLabel }>
                            Zamów
                        </Grid>
                        <Grid item
                              xs={ 1 }>
                            <ShoppingCartIcon />
                        </Grid>
                    </Button>
                    <Button size='small'
                            variant='outlined'
                            color='primary'
                            onClick={ addToShoppingCart }>
                        <Grid item
                              xs={ 11 }
                              className={ classes.buttonLabel }>
                            Dodaj do koszyka
                        </Grid>
                        <Grid item
                              xs={ 1 }>
                            <AddShoppingCartIcon />
                        </Grid>
                    </Button>
                </Grid>
            </Grid>
        </Card>
    </Grid>
);

export default withStyles(
    styles
)( BookDetail );
