import React from 'react';
import { func } from 'prop-types';
import { connect } from 'react-redux';
import { pipe } from 'ramda';
import { withStyles } from '@material-ui/core';

import { Field, Form, reduxForm } from 'redux-form';
import Button from '@material-ui/core/Button/Button';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem/MenuItem';

import { FormField, FormSelect } from '../../../../common/components/form-elements';

const styles = () => ({
    field: {
        width: '100%'
    }
});

const Search = ({ handleSubmit, submitHandler, classes }) => (
    <Form onSubmit={ handleSubmit( submitHandler ) }>
        <Grid container
              direction='row'
              alignItems='flex-end'>
            <Grid item
                  xs={1}>
                <Field name='filterName'
                       component={ FormSelect }
                       label='Filtr'>
                    <MenuItem value={ 'title' }>Tytuł</MenuItem>
                    <MenuItem value={ 'author' }>Autor</MenuItem>
                </Field>
            </Grid>
            <Grid item
                  xs={true}>
                <Field name='search'
                       className={ classes.field }
                       label='Szukaj...'
                       component={ FormField } />
            </Grid>
            <Grid item
                  xs={1}>
                <Button size='small'
                        color='primary'
                        type='submit'>
                    Szukaj
                </Button>
            </Grid>
        </Grid>
    </Form>
);

Search.propTypes = {
    handleSubmit: func.isRequired,
    submitHandler: func.isRequired
};

const mapStateToProps = () => ({
    initialValues: { filterName: 'title' }
});

export default pipe(
    reduxForm({
        form: 'search',
    }),

    connect(
        mapStateToProps
    ),

    withStyles(
        styles
    )
)( Search );
