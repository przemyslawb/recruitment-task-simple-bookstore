import React from 'react';
import { Link } from 'react-router-dom';

import Badge from '@material-ui/core/Badge';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import IconButton from '@material-ui/core/IconButton';

const CartBadge = ({ shoppingCartLength, url }) => (
    <IconButton aria-label='Cart'
                component={ Link }
                to={ url }>
        <Badge badgeContent={ shoppingCartLength }
               color='primary'>
            <ShoppingCartIcon />
        </Badge>
    </IconButton>
);

export default CartBadge;
