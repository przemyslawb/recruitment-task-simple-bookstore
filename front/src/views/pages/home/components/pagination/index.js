import React from 'react';
import { func, number } from 'prop-types';

import ReactPaginate from 'react-paginate';

import './pagination.css';

const Pagination = ({ onPageClick, pageCount }) => (
    <ReactPaginate previousLabel='Poprzednia strona'
                   nextLabel='Następna strona'
                   breakLabel={ <p>...</p> }
                   pageCount={ pageCount }
                   pageRangeDisplayed={ 5 }
                   onPageChange={ onPageClick }
                   containerClassName='pagination'
                   activeClassName='active' />
);

Pagination.propTypes = {
    onPageClick: func.isRequired,
    pageCount: number.isRequired
};

export default Pagination;
