import React, { PureComponent, Fragment } from 'react'
import { isNil, inc } from 'ramda';
import { string, number, arrayOf, shape } from 'prop-types';
import { withStyles } from '@material-ui/core';
import { pipe } from 'ramda';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import { booksOperations } from '../../../state/ducks/books';
import { shoppingCartOperations } from '../../../state/ducks/shopping-cart';
import { shoppingCartSelectors } from '../../../state/ducks/shopping-cart';
import withSpinner from '../../../common/hocs/with-spinner';

import Search from './components/search';
import Pagination from './components/pagination';
import BookDetail from './components/book-detail';
import Popup from '../../../common/components/popup';
import CartBadge from  './components/cart-badge';

const styles = () => ({
    panel: {
        padding: 10,
        marginBottom: 45
    },

    searchInput: {
        width: '50%'
    }
});

class HomeContainer extends PureComponent {
    state = {
        isSnackbarOpened: false,
    };

    // W tym miejscu zamiast odwołania do props można byłby bezpośrednio wstawić akcję, aczkolwiek postawiłem na dodatkową
    // warstwę abstrakcji i na automatyczne dobindowanie do dispatchera, mimo, że jest konieczność użycia arrow function.
    addToShoppingCart = item => () => {
        const { addToShoppingCart } = this.props;

        addToShoppingCart( item );
        this.handleSnackbarOpen();
    };

    submitHandler = values => {
        const {
            fetchBooks
        } = this.props;

        const {
            filterName,
            search
        } = values;

        const params = {
            search: {
                [ filterName ]: search
            }
        };

        fetchBooks( params );
    };

    handlePageClick = ({ selected }) => {
        const {
            fetchBooks
        } = this.props;

        fetchBooks({ page: inc( selected ) });
    };

    handleSnackbarOpen = () => {
        this.setState({ isSnackbarOpened: true });
    };

    handleSnackbarClose = () => {
        this.setState({ isSnackbarOpened: false });
    };

    _getPageCountResult = () => {
        const {
            books: {
                _meta
            },
        } = this.props;

        return _meta.total_records / _meta.records_per_page;
    };

    render() {
        const {
            books: {
                data: books,
            },
            shoppingCartLength,
            classes,
        } = this.props;

        const {
            isSnackbarOpened,
        } = this.state;

        const {
            submitHandler,
            addToShoppingCart,
            handlePageClick,
            handleSnackbarClose,
            _getPageCountResult,
        } = this;

        return (
            <Fragment>
                <Paper className={ classes.panel }>
                    <CartBadge shoppingCartLength={ shoppingCartLength }
                               url='/shopping-cart'/>
                    <Search submitHandler={ submitHandler }
                            className={ classes.searchInput } />
                </Paper>
                <Grid container
                      spacing={ 40 }>
                    {/*
                        W tym miejscu prawdopodobnie należałoby wyenkapsulować renderowanie detalu, również uznałaem, że w tym
                        miejscu, należy przekazywać cały obiekt book, ponieważ paginacja odbywa się po stronie serwera, więc może się
                        zdarzyć stuacja, gdzie niedopasuję ID z koszyka z obecną listą książek.
                    */}
                    { books.map( (book, key) => (
                        <BookDetail key={ key }
                                    book={ book }
                                    addToShoppingCart={ addToShoppingCart( book ) } />
                    ) ) }
                </Grid>
                <Pagination onPageClick={ handlePageClick }
                            pageCount={ _getPageCountResult() } />
                <Popup handleClose={ handleSnackbarClose }
                       message='Dodano do koszyka'
                       isOpen={ isSnackbarOpened } />
            </Fragment>
        )
    }
}

HomeContainer.propTypes = {
    books: shape({
        data: arrayOf(
            shape({
                id: number,
                title: string,
                author: string,
                cover_url: string,
                pages: number,
                price: number,
                currency: string
            })
        ).isRequired,

        _meta: shape({
            page: number,
            records_per_page: number,
            total_records: number
        }).isRequired
    })
};

const mapStateToProps = ({ books: { list }, ...state }) => ({
    books: list,
    shoppingCartLength: shoppingCartSelectors.getLengthItemsInCart( state )
});

const mapDispatchToProps = {
    fetchBooks: booksOperations.fetchBooks,
    addToShoppingCart: shoppingCartOperations.addToShoppingCart
};

const withDataParams = {
    isLoadingAction: ({ books: { isPending } }) => isPending,
    isEmptyAction: ({ books: { data } }) => isNil( data ),
    fetch: ({ fetchBooks }) => fetchBooks()
};

export default pipe(
    withStyles( styles ),

    withSpinner(
        withDataParams,
        mapStateToProps,
        mapDispatchToProps
    )
)( HomeContainer );
