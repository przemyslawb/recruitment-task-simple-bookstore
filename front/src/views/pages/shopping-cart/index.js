import React, { PureComponent } from 'react'
import { connect } from 'react-redux';
import { pipe } from 'ramda';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import CardTable from './components/cart-table';

import { shoppingCartSelectors } from '../../../state/ducks/shopping-cart';

import OderStepper from '../../../common/components/order-stepper'
import { ORDER_STEPS } from '../../../common/constants';
import { orderStepperOperations } from '../../../state/ducks/order-stepper';

const styles = () => ({
    container: {
        minHeight: '59vh'
    },
    tableContainer: {
        width: '100%'
    }
});

class ShippingCardContainer extends PureComponent {
    handleBack = () => {
        const { updateStep, orderStepper } = this.props;

        const prevStep = orderStepper - 1;

        updateStep( prevStep );
    };

    handleNext = () => {
        const { updateStep, orderStepper } = this.props;

        const nextStep = orderStepper + 1;

        updateStep( nextStep );
    };

    render() {
        const {
            books,
            orderStepper,
            classes
        } = this.props;

        const {
            handleNext
        } = this;

        return (
            <Grid container
                  className={ classes.container }
                  alignItems='center'
                  alignContent='space-between'
                  spacing={ 40 }>
                <OderStepper activeStep={ orderStepper }
                             steps={ ORDER_STEPS }>
                    <Paper elevation={ 1 }
                           className={ classes.tableContainer }>
                        <CardTable books={ books }  />
                    </Paper>
                    <div>
                        <Button size='small'
                                color='primary'
                                component={ Link }
                                to='/'>
                            Wróć do księgarni
                        </Button>
                        <Button size='small'
                                color='primary'
                                component={ Link }
                                onClick={ handleNext }
                                to='/order-summary'>
                            Dalej
                        </Button>
                    </div>
                </OderStepper>
            </Grid>
        )
    }
}

const mapStateToProps = state => ({
    books: shoppingCartSelectors.getCalculatedBooksFromCart( state ),
    orderStepper: state.orderStepper.actualPosition
});

const mapDispatchToProps = {
    updateStep: orderStepperOperations.updateStep
};

export default pipe(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),

    withStyles( styles )
)( ShippingCardContainer );
