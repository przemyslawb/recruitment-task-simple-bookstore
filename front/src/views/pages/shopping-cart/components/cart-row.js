import React from 'react';
import { withStyles } from '@material-ui/core';

import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import CurrencyFormatter from '../../../../common/components/currency-formatter';

const styles = () => ({
    image: {
        width: 'auto',
        height: 70
    },
    productInfo: {
        margin: 15
    }
});

const CartRow = ({ id, title, quantity, priceSum, author, cover_url, classes }) => (
    <TableRow key={ id }>
        <TableCell component='th'
                   scope='row'>
            <Grid container
                  alignItems='center'>
                <img src={ cover_url }
                     className={ classes.image }
                     alt={ title } />
                <div className={ classes.productInfo }>
                    <Typography component='p'>
                        { title }
                    </Typography>
                    <Typography variant='caption'>
                        { author }
                    </Typography>
                </div>
            </Grid>
        </TableCell>
        <TableCell numeric>{ quantity }</TableCell>
        <TableCell numeric>
            <CurrencyFormatter quantity={ priceSum } />
        </TableCell>
    </TableRow>
);

export default withStyles(
    styles
)( CartRow );
