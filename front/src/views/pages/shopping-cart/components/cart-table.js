import React from 'react';

import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import Table from '@material-ui/core/Table';

import CardRow from './cart-row';

const CartTable = ({ books }) => (
    <Table>
        <TableHead>
            <TableRow>
                <TableCell>Produkt</TableCell>
                <TableCell numeric>Ilość</TableCell>
                <TableCell numeric>Cena</TableCell>
            </TableRow>
        </TableHead>
        <TableBody>
            { books.map(book => ( <CardRow key={ book.id } { ...book } /> )) }
        </TableBody>
    </Table>
);

export default CartTable;
