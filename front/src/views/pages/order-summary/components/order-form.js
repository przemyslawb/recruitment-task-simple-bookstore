import React from 'react'

import { Field, Form, reduxForm } from 'redux-form';

import Button from '@material-ui/core/Button/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { FormField } from '../../../../common/components/form-elements';
import {
    required,
    emailValidation,
    zipCodeValidation,
    lengthValidation
} from '../../../../common/form-validators';

const fields = {
    contact: [
        {
            name: 'first_name',
            label: 'Imię',
            component: FormField,
            validate: [ required ]
        },
        {
            name: 'last_name',
            label: 'Nazwisko',
            component: FormField,
            validate: [ required ]
        },
        {
            name: 'email',
            label: 'E-mail',
            type: 'email',
            component: FormField,
            validate: [ required, emailValidation ]
        },
        {
            name: 'phone_number',
            label: 'Nr telefonu',
            component: FormField,
            validate: [ required, lengthValidation( 9 ) ]
        }
    ],

    address: [
        {
            name: 'address',
            label: 'Adres',
            component: FormField,
            validate: [ required ]
        },
        {
            name: 'city',
            label: 'Miasto',
            component: FormField,
            validate: [ required ]
        },
        {
            name: 'zip_code',
            label: 'Kod',
            component: FormField,
            validate: [ required, zipCodeValidation ]
        }
    ]
};

const renderField = (field, index) => (
    <Grid item xs={ 4 }
          key={ index }>
        <Field { ...field } />
    </Grid>
);

const OrderForm = ({ handleSubmit, submitHandler, children, valid }) => (
    <Form onSubmit={ handleSubmit( submitHandler ) }>
        <Grid container
              direction='row'>
            <Grid item
                  xs={ 6 }>
                <Typography variant='subtitle1'>
                    Dane kontaktowe
                </Typography>
                { fields.contact.map( renderField ) }
            </Grid>
            <Grid item
                  xs={ 6 }>
                <Typography variant='subtitle1'>
                    Adres
                </Typography>
                { fields.address.map( renderField ) }
            </Grid>
        </Grid>
        { children }
        <Button size='small'
                disabled={ !valid }
                color='primary'
                type='submit'>
            Zamów
        </Button>
    </Form>
);


export default reduxForm({
    form: 'order'
})( OrderForm );
