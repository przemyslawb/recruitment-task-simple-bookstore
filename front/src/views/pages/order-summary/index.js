import React, { PureComponent } from 'react'
import { connect } from 'react-redux';
import { pipe, dec } from 'ramda';
import { reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';

import OrderForm from './components/order-form';
import { ORDER_STEPS } from '../../../common/constants';
import OderStepper from '../../../common/components/order-stepper';
import { orderStepperOperations } from '../../../state/ducks/order-stepper';
import { orderOperations } from '../../../state/ducks/order';
import { shoppingCartOperations } from '../../../state/ducks/shopping-cart';
import Popup from '../../../common/components/popup';

const styles = () => ({
    container: { minHeight: '59vh' },
    content: {
        width: '100%',
        marginTop: 80,
        padding: 30
    }
});

class OrderSummaryContainer extends PureComponent {
    state = {
        isSnackbarOpened: false,
    };

    submitHandler = async values => {
        const {
            books,
            submitOrder
        } = this.props;

        const {
            handleSnackbarOpen,
            actionsOnSuccessSubmit
        } = this;

        const data = {
            ...values,
            order: books
        };

        try {
            await submitOrder( data );
            handleSnackbarOpen();
            actionsOnSuccessSubmit();
        } catch (error) {
            throw new Error( error );
        }
    };

    actionsOnSuccessSubmit = () => {
        const {
            updateStep,
            clearShoppingCart,
            history: { push: redirect }
        } = this.props;

        const RESET_STEPS = 0;
        // Mały hack.
        setTimeout(() => {
            updateStep( RESET_STEPS );
            clearShoppingCart();
            redirect( '/' );
        }, 1500);
    };

    handleSnackbarOpen = () => {
        this.setState({ isSnackbarOpened: true });
    };

    handleSnackbarClose = () => {
        this.setState({ isSnackbarOpened: false });
    };

    handleBack = () => {
        const { updateStep, orderStepper } = this.props;

        const prevStep = dec( orderStepper );

        updateStep( prevStep );
    };

    render() {
        const {
            orderStepper,
            classes
        } = this.props;

        const {
            isSnackbarOpened
        } = this.state;

        const {
            handleSnackbarClose
        } = this;

        return (
            <Grid container
                  className={ classes.container }
                  alignItems='center'
                  alignContent='space-around'>
                <OderStepper activeStep={ orderStepper }
                             steps={ ORDER_STEPS }>
                    <Paper className={ classes.content }>
                        <OrderForm submitHandler={this.submitHandler} >
                        <Button size='small'
                                color='primary'
                                component={ Link }
                                onClick={ this.handleBack }
                                to='/shopping-cart'>
                            Cofnij
                        </Button>
                        </OrderForm>
                    </Paper>
                </OderStepper>
                <Popup handleClose={ handleSnackbarClose }
                       message='Złożono zamówienie'
                       isOpen={ isSnackbarOpened } />
            </Grid>
        )
    }
}

const mapStateToProps = ({ shoppingCart, orderStepper }) => ({
    books: Object.values( shoppingCart.quantityById ).map(({ id, quantity }) => ({ id, quantity })),
    orderStepper: orderStepper.actualPosition
});

const mapDispatchToProps = {
    updateStep: orderStepperOperations.updateStep,
    clearShoppingCart: shoppingCartOperations.clearShoppingCart,
    submitOrder: orderOperations.submitOrder
};

export default pipe(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),

    reduxForm({
        form: 'order'
    }),

    withStyles( styles )
)( OrderSummaryContainer );
