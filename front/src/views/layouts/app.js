import React, { Fragment } from 'react';
import classNames from 'classnames';
import { Route, Switch } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';

import CssBaseline from '@material-ui/core/CssBaseline';

import Header from './components/header';
import Footer from './components/footer';
import routes from '../../routes';

const styles = theme => ({
    icon: {
        marginRight: theme.spacing.unit * 2,
    },

    header: {
        backgroundColor: theme.palette.background.paper,
    },

    headerContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
    },

    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: 'auto',
            marginRight: 'auto',
        },

        padding: `${theme.spacing.unit * 8}px 0`,
    },

    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing.unit * 6,
    },
});

const renderRoutes = (route, key) => (
    <Route key={ key } { ...route } />
);

// Na potrzeby tej aplikacji główny komponent jest bez stanowy, ponieważ logika jest w entry components.
const App = ({ classes: { layout, cardGrid, ...classes } }) => (
    <Fragment>
        <CssBaseline />
        <Header classes={ classes } />
        <main className={ classNames(layout) }>
            <Switch>
                { routes.map( renderRoutes ) }
            </Switch>
        </main>
        <Footer classes={ classes } />
    </Fragment>
);

export default withStyles(
    styles
)( App );
