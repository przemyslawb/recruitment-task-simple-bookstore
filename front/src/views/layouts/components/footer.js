import React from 'react';
import { object } from 'prop-types';

import Typography from '@material-ui/core/Typography/Typography';

const Footer = ({ classes: { footer } }) => (
    <footer className={ footer }>
        <Typography variant='h6'
                    align='center'
                    gutterBottom>
            Edu Księgarnia
        </Typography>
        <Typography variant='subtitle1'
                    align='center'
                    color='textSecondary'
                    component='p'>
            Najlepszy sklep z książkami edukacyjymi :)
        </Typography>
    </footer>
);

Footer.propTypes = {
    classes: object.isRequired
};

export default Footer;
