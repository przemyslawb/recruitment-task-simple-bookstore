import React from 'react';
import { object } from 'prop-types';

import Typography from '@material-ui/core/Typography';

const Header = ({ classes: { header, headerContent } }) => (
    <header className={ header }>
        <div className={ headerContent }>
            <Typography component='h1'
                        variant='h2'
                        align='center'
                        color='textPrimary'
                        gutterBottom>
                Edu Księgarnia
            </Typography>
        </div>
    </header>
);

Header.propTypes = {
    classes: object.isRequired
};

export default Header;
