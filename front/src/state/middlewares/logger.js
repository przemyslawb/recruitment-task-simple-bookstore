import { createLogger } from 'redux-logger';

const settings = {
    duration: true,
    collapsed: true,
    diff: true
};

const logger = process.env.NODE_ENV === 'development' ? createLogger( settings ) : () => {};

export default logger;
