import logger from './logger'
import { requestsPromiseMiddleware } from 'redux-saga-requests';

const middlewares = [
    logger,
    requestsPromiseMiddleware()
];

export default middlewares;
