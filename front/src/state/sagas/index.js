import {
    createRequestInstance,
    watchRequests
} from 'redux-saga-requests';

import { createDriver } from 'redux-saga-requests-axios';

import {
    booksTypes
} from '../ducks/books';

export default function* rootSaga(axiosInstance) {
    yield createRequestInstance({
        driver: createDriver( axiosInstance )
    });

    // W tym miejscu potencjalnie jest słabe skalowanie, ale z racji presji czasu wybrałem tą metodykę.
    yield watchRequests(null, {
        [ booksTypes.FETCH_BOOKS ]: { abortOn: booksTypes.CLEAR_BOOKS },
    });
}
