import { combineReducers } from 'redux';

import { createReducer } from '../../../common/utils'
import { UPDATE_STEP } from './types';

const orderStepReducer = createReducer(0, {
    [ UPDATE_STEP ]: (state, action) => action.step
});

export default combineReducers({
    actualPosition: orderStepReducer
});
