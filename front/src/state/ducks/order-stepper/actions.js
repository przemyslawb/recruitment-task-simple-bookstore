import { makeActionCreator } from '../../../common/utils';
import { UPDATE_STEP } from './types';

export const updateStep = makeActionCreator( UPDATE_STEP, 'step' );
