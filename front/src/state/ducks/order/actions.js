import api from '../../../api';

import {
    SUBMIT_ORDER
} from './types';

export const submitOrder = data => ({
    type: SUBMIT_ORDER,
    request: {
        url: api.order.submitOrder(),
        method: 'POST',
        data
    },
    meta: {
        asPromise: true,
    }
});
