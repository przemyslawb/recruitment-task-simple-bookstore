import { combineReducers } from 'redux';
import { requestsReducer } from 'redux-saga-requests';

import {
    SUBMIT_ORDER
} from './types';

const orderReducer = requestsReducer({ actionType: SUBMIT_ORDER });

export default combineReducers({
    details: orderReducer
});
