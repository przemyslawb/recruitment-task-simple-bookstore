import { combineReducers } from 'redux';
import { success, error, abort } from 'redux-saga-requests';

import { createReducer } from '../../../common/utils';

import {
    CLEAR_BOOKS,
    FETCH_BOOKS
} from './types';

const initialState = {
    data: null,
    isFetching: false,
    error: false,
};

const booksReducer = createReducer(initialState, {
    [ FETCH_BOOKS ]: state => ({
        ...state,
        isFetching: true
    }),

    [ success( FETCH_BOOKS ) ]: (state, { data: { data, metadata: _meta } }) => ({
        ...state,
        data,
        isFetching: false,
        _meta
    }),

    [ error( FETCH_BOOKS ) ]: state => ({
        ...state,
        isFetching: false,
        error: true
    }),

    [ abort( CLEAR_BOOKS ) ]: state => ({
        ...state,
        isFetching: false
    })
});

export default combineReducers({
    list: booksReducer
});
