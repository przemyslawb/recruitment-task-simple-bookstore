import api from '../../../api';

import {
    CLEAR_BOOKS,
    FETCH_BOOKS
} from './types';

export const fetchBooks = (params = {}) => ({
    type: FETCH_BOOKS,
    request: {
        url: api.books.getAll(),
        params
    }
});

export const clearBooks = () => ({ type: CLEAR_BOOKS });
