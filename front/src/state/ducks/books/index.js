import reducers from './reducers';
import * as booksOperations from './actions';
import * as booksTypes from './types';

export {
    booksOperations,
    booksTypes
};

export default reducers
