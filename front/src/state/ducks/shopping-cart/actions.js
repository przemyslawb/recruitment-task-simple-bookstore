import { makeActionCreator } from '../../../common/utils';
import { ADD_TO_SHOPPING_CART, CLEAR_SHOPPING_CART } from './types';

export const addToShoppingCart = makeActionCreator( ADD_TO_SHOPPING_CART, 'book' );
export const clearShoppingCart = makeActionCreator( CLEAR_SHOPPING_CART );
