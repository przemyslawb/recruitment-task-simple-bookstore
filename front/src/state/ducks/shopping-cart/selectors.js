import { createSelector } from 'reselect';
import { reduce, add, pluck, values, prop, curry } from 'ramda';

const cartItemSelector = state => state.shoppingCart.quantityById;

export const getCalculatedBooksFromCart = createSelector(
    cartItemSelector,
    itemsInCart => Object
        .values( itemsInCart )
        .map( book => {
            const quantity = prop( 'quantity', book );
            const priceSum = prop( 'price', book ) * quantity;

            return {
                quantity,
                priceSum,
                ...book
            };
        } )
);

const sumValuesBy = curry(
    (prop, items) => reduce(
        add, 0, pluck( prop, values(items) )
    )
);

export const getLengthItemsInCart = createSelector(
    cartItemSelector,
    sumValuesBy( 'quantity' )
);
