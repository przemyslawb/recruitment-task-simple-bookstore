import { combineReducers } from 'redux';
import { ifElse, inc, assoc, has } from 'ramda';

import { createReducer } from '../../../common/utils'
import {
    ADD_TO_SHOPPING_CART,
    CLEAR_SHOPPING_CART
} from './types';

const initialState = {};

const shoppingCartReducer = createReducer(initialState, {
    [ ADD_TO_SHOPPING_CART ]: (prevState, { book }) => ifElse(
        has( book.id ),
        state => ({
            ...state,
            [ book.id ]: {
                ...state[ book.id ],
                ...book,
                quantity: inc( state[ book.id ].quantity )
            }
        }),
        assoc( book.id, { quantity: 1, ...book } ),
    )( prevState ),

    [ CLEAR_SHOPPING_CART ]: () => initialState
});

export default combineReducers({
    quantityById: shoppingCartReducer
});
