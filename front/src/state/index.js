import {
    createStore,
    applyMiddleware,
    combineReducers,
    compose
} from 'redux';

import createSagaMiddleware from 'redux-saga';

import rootSaga from './sagas'
import * as reducers from './ducks'
import axios from '../api/axios-wrapper'
import middlewares from './middlewares'

const combinedReducers = combineReducers( reducers );
const sagaMiddleware = createSagaMiddleware();

// Poniższa funkcja powinna być bardziej zenkapsulowana, ale niestety z racji presji czasu postawiłem na to rozwiązanie.
export const configureStore = () => {
    const composeEnhancers =
        (typeof window !== 'undefined' &&
            window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
        compose;

    const store = createStore(
        combinedReducers,
        composeEnhancers(
            applyMiddleware(
                sagaMiddleware,
                ...middlewares
            )
        ),
    );

    sagaMiddleware.run( rootSaga, axios );

    return store;
};
