import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { configureStore } from './state/index'

import dotenv from 'dotenv';
import App from './views/layouts/app';

dotenv.config();

const store = configureStore();

const Root = () => (
    <Provider store={ store }>
        <Router>
            <App />
        </Router>
    </Provider>
);

render(
    <Root />,
    document.getElementById( 'root' )
);
