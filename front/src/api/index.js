// Prosta warstwa abstrakcji na REST API;

const api = {
    books: {
        getAll: () => '/book'
    },
    order: {
        submitOrder: () => '/order'
    }
};

export default api;
