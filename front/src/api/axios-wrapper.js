import axios from 'axios';
import { stringify } from 'qs';

const {
    REACT_APP_API_URL: baseURL
} = process.env;

const instance = axios.create({
    baseURL,
    paramsSerializer(params) {
        return stringify(
            params,
            { arrayFormat: 'brackets' }
        );
    }
});

export default instance;
